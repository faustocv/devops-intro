class: center, middle, main-title

# DevOps: Introduction and Experiences

By FaustoC

*fcastane@thoughtworks.com*

???

Good afernoon everyone.
My name is Fausto Castaneda, but you can call me as FaustoC.
That's because there is other person with the same name.

I am a developer at Thoughtworks. I have experience in full stack development but Ihave a strong focus over DevOps culture and IoT tecnologies.

Also, I love VIM. It's my preferred text editor. And I love Docker and cloud tecnologies.

Today, I'd like to share with you about some basic ideas about DevOps and our experiences around.

Let me quickly take you through today's presentation.

First, I'm going to talk about our experiences in DevOps culture.

After that, we'll be taking a look at DevOps concepts and principles.

Finally, I'll explain about commonly anti-types practices of DevOps structures.

So, Let's start.

---

class: center, middle, subtitle-1

# An Airlines firm ...

???
I'd like to start by sharing with you our experience in DevOps with an airlines company two years ago.

---

## DevOps Experience

.center[![devops_experience](./assets/devops_experience_1.png)]

???

This company has an IT departament. That departament whas in charge of developing software applications and managing the data center.

The IT departament is divided into the development area and the operations area.

The development area followed an agile approach whereas the operations area followed and COBIT/ITIL approach.

The development area is formed by several agile teams. Those teams applied good practices like CI/CD, Pipeline as Code, TDD, etc.

The operations area is responsible for applications releases and managing environments like Beta, Staging and Prod. Also, It keeps availability of software applications too.

Both areas were very good doing their tasks and mananging their resources. Nevertheless, there was a gap between them. There was not a good communication neither.

---

## DevOps Experience

.center[![devops_experience](./assets/devops_experience_2.png)]

???

What happend when the development team has finished its work and it's wants to do a new release.

The way to do a new release is asking operations team.

---

## DevOps Experience

.center[![devops_experience](./assets/devops_experience_3.png)]

???

Operations team takes the development team's artifact and deploy it at Beta environment first. If there is not errors in Beta enviroment, then the artifact is deployed at Stating and so on, until to arrive at production.


---

## DevOps Experience

.center[![devops_experience](./assets/devops_experience_4.png)]

---

## DevOps Experience

.center[![devops_experience](./assets/devops_experience_5.png)]

---

## DevOps Experience

.center[![devops_experience](./assets/devops_experience_6.png)]

???

This process was manually.

If while the process there were no errors, the time for doing a release takes 15 days.

It was a lot of time.

It was becomes a real issue to development and operation teams and the company too.

---

.center[![devops_experience](./assets/devops_experience_7.png)]

???

Going from bad to worse. This company has several development teams. Every team has differents requirement regarded to infrastructure. Inclusive, one of them needs to crrate a new database. This database should be installed in all environments. And so on.

---

class: center, middle, subtitle-2

# So, how did we work with them?

---

## DevOps Experience

.center[![devops_experience](./assets/devops_experience_8.png)]

???

First at all. We worked on building a better communication between both teams.

We formed a temporally group with members of development team, operations team, and two developers of TWs.

After we understood the requeriments of all stakeholders, we helped to improve the current pipeline.

The main aim was to bridge the gap between development and operations phase.

We added new tasks for static analysis, tests, uploading to repositories, automatic deployment for each enviroment.

So, the idea with this  pipeline is to provide a truly Continuous Delivery. Every change in the code is a candidate for production release. There is no manual intervention neither manipulation of the artifact. The product owner decides which artifact should be released.

The time for release was improved. Now, it only takes 15 minutes.
No more manual manipulation.

But, still there is remaining work. Did you notice the differences between Beta, Staging and Production environments?

---

## DevOps Experience

.center[![devops_experience](./assets/devops_experience_9.png)]

???

Well, we solved that inconvenient using Docker.

We modified the pipeline for adding Docker support.

Now the artifact is an image. The three environments are very similar.

It's important to mention that the development team is the owner of the pipeline.

The team is responsible for monitoring the application in production too.

---


class: center, middle, subtitle-2

# And, how did the story end?

---

## Outcomes

<br/>

| .key-word[Dev] | .normal-word[Ops] |
| ---                                        | ---                                    |
| Reduction in lead time                     | Fewer downtimes                         |
| Better app integration with infrastructure | Less unplanned work. No put out fires. |
| Daily releases                             | Strategy focus in infrastructure       |
| Motivated teams                            | Motivated teams                        |

---

class: center, middle, subtitle-3

# So, what does DevOps mean?

---

## DevOps Definition

<br/>

.left[.normal-word[Cultural movement]]

.center[.key-word[To bridge the gap between development and operations]]

.right[.normal-word[To reduce risk in development software]]

.right[.image-source[Stephen Nelson-Smith, What is this DevOps thing anyway?]]

???
* Movement born in 2009

---

class: center, middle, subtitle-1

# Principles of DevOps: PPT

---

## Principles of DevOps: PPT

| .key-word[People] | .normal-word[Product] | .key-word[Tools] |
| ---               | ---                | ---              |
| People experience OVER tools | Delivering small batches with continuous experiments INSTEAD OF big batches | Automation OVER manual work |
| Holistic approach OVER departmental silos | Customer feedback OVER meeting work deadlines| Infrastructure as code OVER manual configuration |
| Products OVER projects | Independent applications OVER monolithic application | Quality throughout the process INSTEAD OF the late inspection against metrics |
| Learning from failure INSTEAD OF blaming others |               | Information radiators INSTEAD OF lengthy reports|

.right[.image-source[Bin Wu, Consultant, TW China]]

---

class: center, middle, subtitle-2

# DevOps Anti-Types

---

.center[![devops_antitypes](./assets/devops_antitypes.png)]

---

class: center, middle, main-title

# Thanks
More info:

*Matthew Skelton*, "DevOps Topologies", web.devopstopologies.com

*Katrina Clokie*, "A Practical Guide to Testing in DevOps"
